
# OCDS Compiled Release API

This is an unofficial REST API to create an OCDS compiled release using OCDS releases with same OCID as input.

If you want to use the official command line interface tool with all OCDS features please use
[ocdskit](https://github.com/open-contracting/ocdskit) from the [Open Contracting Partnership](https://www.open-contracting.org/).

## Install

docker compose up api --build -d

## Environment variables

* TZ: Time zone used by API
* API_PORT: The host-side API port

## How to use

Send a HTTP POST request to the API filling the next json payload.

POST http://localhost:8081/compiled/
```
{
  "version": "fill this string with the ocds version",
  "release_base_url": "fill this string with the release api endpoint",
  "extensions": [
    "fill this string array with the ocds extension"
  ],
  "releases": [
    {}
  ]
}
```


Then the API will return an OCDS record with this structure.
```
{
	"compiledRelease": {
	},
	"ocid": "release.ocid value",
	"releases": [
		{
			"url": "relase_base_url + release.id value",
			"date": "release.date value",
			"tag": [
				"release.tag value"
			]
		}
	]
}
```

You can also check a curl request example [here](./tests/compiled_endpoint.sh).

## License

See the content of [LICENSE](./LICENSE) file.

## Third-party license

See the content of [THIRD-PARTY-LICENSE](./THIRD-PARTY-LICENSE) file.