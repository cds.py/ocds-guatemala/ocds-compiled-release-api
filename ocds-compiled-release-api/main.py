from fastapi import FastAPI
from model import Item
from util import create_compiled_release


app = FastAPI()

@app.post("/compiled/")
def compiled(item: Item) -> dict:
    return create_compiled_release(
        release_base_url=item.release_base_url,
        version=item.version,
        extensions=item.extensions,
        releases=item.releases
    )

@app.get("/")
def root():
    return {'status': 'on'}
