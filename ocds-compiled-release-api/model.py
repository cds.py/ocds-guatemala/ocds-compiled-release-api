from pydantic import BaseModel


class Item(BaseModel):
    version: str
    release_base_url: str
    extensions: list[str]
    releases: list[dict]
