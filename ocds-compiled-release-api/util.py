
import os

from ocdskit.combine import merge
from libcoveocds.schema import SchemaOCDS


def get_extended_schema(version, extensions):
    path = os.path.join(os.getcwd(), 'extended_schema.json')
    if not os.path.exists(path):
        schema = SchemaOCDS(package_data={'version': version, 'extensions': extensions})
        schema.create_extended_schema_file('','')
        path = schema.extended_schema_file
    return path

def create_compiled_release(release_base_url, releases, version, extensions):
    compiledRelease = next(merge(data=releases, schema=get_extended_schema(version, extensions)), None)

    if compiledRelease == None:
        return None

    return {
        'compiledRelease': compiledRelease,
        'ocid': compiledRelease['ocid'],
        'releases': [
            {
                'url': f"{release_base_url}{release['id']}",
                'date': release['date'],
                'tag': release['tag']
            } for release in releases
        ]
    }
