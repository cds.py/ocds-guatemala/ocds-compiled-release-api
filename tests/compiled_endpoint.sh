#!/bin/bash

curl --request POST \
  --url http://localhost:8081/compiled/ \
  --header 'Content-Type: application/json' \
  --data '{
  "release_base_url": "https://gcconsultanog-capa.guatecompras.gt/gc-api/release/",
  "version": "1.1",
  "extensions": [
   "https://raw.githubusercontent.com/open-contracting-extensions/ocds_bid_extension/master/extension.json",
    "https://raw.githubusercontent.com/open-contracting-extensions/ocds_memberOf_extension/master/extension.json",
    "https://gitlab.com/dncp-opendata/ocds_item_attributes_extension/raw/master/extension.json",
    "https://raw.githubusercontent.com/open-contracting-extensions/ocds_additionalContactPoints_extension/master/extension.json",
    "https://gitlab.com/dncp-opendata/ocds_statusdetails_extension/-/raw/master/extension.json",
    "https://gitlab.com/dncp-opendata/ocds_document_type_details/-/raw/master/extension.json",
    "https://raw.githubusercontent.com/portaledcahn/ocds_tenderDatePublished_extension/master/extension.json",
    "https://raw.githubusercontent.com/guatecompras/ocds_partyDetails_publicEntitiesLevelDetails_extension/main/extension.json",
    "https://bitbucket.org/ONCAETI/ocds_releasesource_extension/raw/master/extension.json",
    "https://devops11.osce.gob.pe/bitbucket/projects/PROYEC/repos/ocds_releasepublisheddate_extension/raw/extension.json",
    "https://devops11.osce.gob.pe/bitbucket/projects/PROYEC/repos/ocds_datasegmentation_extension/raw/extension.json"
  ],
  "releases": [
    {
      "ocid": "ocds-xqjsxa-17326044",
      "date": "2022-06-02T15:59:26",
      "publishedDate": "2023-11-17T16:55:50-06:00",
      "language": "es",
      "tag": [
        "tender",
        "award"
      ],
      "initiationType": "tender",
      "parties": [
        {
          "identifier": {
            "scheme": "GT-CISP",
            "id": "12100404"
          },
          "address": {
            "streetAddress": "1ra. Avenida y 1ra. Calle Zona 1 Comalapa",
            "locality": "SAN JUAN COMALAPA",
            "region": "CHIMALTENANGO",
            "countryName": "GUATEMALA"
          },
          "contactPoint": {
            "name": "MUNICIPALIDAD DE COMALAPA, CHIMALTENANGO",
            "email": "jpalacios@minfin.gob.gt",
            "telephone": "7849-8101 ó 7849-8651",
            "faxNumber": "7849-8650"
          },
          "roles": [
            "buyer"
          ],
          "additionalContactPoints": [
            {
              "name": "MUNICIPALIDAD DE COMALAPA, CHIMALTENANGO",
              "email": "afolgar@minfin.gob.gt"
            }
          ],
          "details": {
            "entityType": {
              "description": "Gobiernos Locales (Municipalidades, Mancomunidades, etc.)",
              "id": "6"
            },
            "type": {
              "description": "Sector Público",
              "id": "1"
            }
          },
          "id": "GT-CISP-12100404",
          "name": "MUNICIPALIDAD DE COMALAPA, CHIMALTENANGO"
        },
        {
          "identifier": {
            "scheme": "GT-GCUC",
            "id": "192-3"
          },
          "address": {
            "streetAddress": "0 Avenida y 1ra. calle zona 1.  San Juan Comalapa Chimaltenango.",
            "locality": "SAN JUAN COMALAPA",
            "region": "CHIMALTENANGO",
            "countryName": "GUATEMALA"
          },
          "contactPoint": {
            "name": "MUNI COMALAPA",
            "email": "jpalacios@minfin.gob.gt",
            "telephone": "78498101",
            "faxNumber": "78498101"
          },
          "roles": [
            "buyer"
          ],
          "additionalContactPoints": [
            {
              "name": "MUNI COMALAPA",
              "email": "afolgar@minfin.gob.gt"
            }
          ],
          "memberOf": [
            {
              "id": "GT-CISP-12100404",
              "name": "MUNICIPALIDAD DE COMALAPA, CHIMALTENANGO"
            }
          ],
          "details": {
            "entityType": {
              "description": "Gobiernos Locales (Municipalidades, Mancomunidades, etc.)",
              "id": "6"
            },
            "type": {
              "description": "Sector Público",
              "id": "1"
            }
          },
          "id": "GT-GCUC-192-3",
          "name": "MUNI COMALAPA"
        },
        {
          "identifier": {
            "scheme": "GT-GCID",
            "id": "-91CDDD235F7BA83C6D43F4DD32043919"
          },
          "roles": [
            "tenderer",
            "supplier"
          ],
          "id": "GT-GCID--91CDDD235F7BA83C6D43F4DD32043919",
          "name": "EQUIPOS Y SOLUCIONES DE LIMPIEZA, SOCIEDAD ANONIMA"
        },
        {
          "identifier": {
            "scheme": "GT-GCID",
            "id": "-7E2C6F5B67F795E10AE2960912223414"
          },
          "roles": [
            "tenderer"
          ],
          "id": "GT-GCID--7E2C6F5B67F795E10AE2960912223414",
          "name": "PABLO RENATO ÁVILA ENRIQUEZ"
        }
      ],
      "buyer": {
        "id": "GT-NIT-6150004",
        "name": "MUNICIPALIDAD DE COMALAPA, CHIMALTENANGO"
      },
      "bids": {
        "details": [
          {
            "date": "2022-06-02T15:52:46-06:00",
            "status": "valid",
            "value": {
              "amount": 49950,
              "currency": "GTQ"
            },
            "tenderers": [
              {
                "id": "GT-GCID--91CDDD235F7BA83C6D43F4DD32043919",
                "name": "EQUIPOS Y SOLUCIONES DE LIMPIEZA, SOCIEDAD ANONIMA"
              }
            ],
            "id": "F48EAB1B231951C874BDE2537742B6D1"
          },
          {
            "date": "2022-05-26T14:36:16-06:00",
            "status": "valid",
            "value": {
              "amount": 49950,
              "currency": "GTQ"
            },
            "tenderers": [
              {
                "id": "GT-GCID--7E2C6F5B67F795E10AE2960912223414",
                "name": "PABLO RENATO ÁVILA ENRIQUEZ"
              }
            ],
            "id": "51B55DFF168D0DA813FFF877D546197A"
          }
        ]
      },
      "tender": {
        "title": "ADQUISICION DE MAQUINA TRITURADORA DE RESIDUOS ORGANICOS PARA LA MUNICIPALIDAD DE COMALAPA, CHIMALTENANGO",
        "status": "complete",
        "statusDetails": "Adjudicado",
        "datePublished": "2022-05-26T12:26:16-06:00",
        "procuringEntity": {
          "id": "GT-NIT-6150004",
          "name": "MUNICIPALIDAD DE COMALAPA, CHIMALTENANGO"
        },
        "items": [
          {
            "description": "Maquinaria Trituradora de Residuos orgánicos (MODELO TR 500G, disco de cortes de 24cm, altura de 1.61M ancho de 1.70m) capacidad Mínima de 3 a 5M3 por hora y con 2 embudo de alimentación.",
            "quantity": 1,
            "unit": {
              "name": "Unidad"
            },
            "attributes": [
              {
                "value": "Obligatorio salvo excepciones",
                "id": "D0815E62E79DF93625F029950B3E9C32",
                "name": "Tipo de obligatoriedad"
              },
              {
                "value": "Maquinaria Trituradora de Residuos orgánicos (MODELO TR 500G, disco de cortes de 24cm, altura de 1.61M ancho de 1.70m) capacidad Mínima de 3 a 5M3 por hora y con 2 embudo de alimentación.",
                "id": "8972DD901DA74E056C2181ECA742A76A",
                "name": "Características"
              },
              {
                "value": "TIEMPO DE CREDITO",
                "id": "00C379996D16B465B8D6F09B7F8B5C84",
                "name": "Características Adicionales"
              },
              {
                "value": "     INCLUIR CERTIFICACION DE CALIDAD VIGENTE",
                "id": "6C6119700EC8894058276D4866647890",
                "name": "Características Adicionales"
              },
              {
                "value": "TIEMPO DE ENTREGA",
                "id": "9E3CAC62222DA6C527044A8B773BA452",
                "name": "Características Adicionales"
              },
              {
                "value": "Garantía de buen funcionamiento",
                "id": "21D9C361A31A71F15E55A3BFCC6DB4D7",
                "name": "Características Adicionales"
              },
              {
                "value": "INCLUIR ESPECIFICACIONES TECNICAS",
                "id": "C46AB5A111A1D9267530CC14AA7EA527",
                "name": "Características Adicionales"
              }
            ],
            "id": "6E513FD872769C3E7E759A212FEE15DB"
          }
        ],
        "procurementMethod": "open",
        "procurementMethodDetails": "Adquisición Directa por Ausencia de Oferta",
        "mainProcurementCategory": "goods",
        "submissionMethod": [
          "written"
        ],
        "tenderPeriod": {
          "startDate": "2022-05-26T12:26:16-06:00",
          "endDate": "2022-06-02T15:59:26-06:00"
        },
        "numberOfTenderers": 0,
        "documents": [
          {
            "title": "Documento que ampara el tipo de compra",
            "url": "http://www.guatecompras.gt/concursos/files/3466/17326044@RES.%20No.%20430-2022_CAMBIO%20DE%20MODALIDAD%20A%20COMPRA%20DIRECTA.pdf",
            "format": "application/pdf",
            "language": "es",
            "datePublished": "2022-05-26T12:26:16-06:00",
            "documentTypeDetails": "Documento que ampara el tipo de compra",
            "id": "2EC2D40ED441497E2C7037D25084F708"
          }
        ],
        "id": "GT-NOG-17326044"
      },
      "awards": [
        {
          "title": "ADQUISICION DE MAQUINA TRITURADORA DE RESIDUOS ORGANICOS PARA LA MUNICIPALIDAD DE COMALAPA, CHIMALTENANGO",
          "status": "active",
          "statusDetails": "Habilitado",
          "date": "2022-06-02T15:59:26-06:00",
          "value": {
            "amount": 49950,
            "currency": "GTQ"
          },
          "suppliers": [
            {
              "id": "GT-GCID--91CDDD235F7BA83C6D43F4DD32043919",
              "name": "EQUIPOS Y SOLUCIONES DE LIMPIEZA, SOCIEDAD ANONIMA"
            }
          ],
          "id": "GT-ADJ-17326044--91CDDD235F7BA83C6D43F4DD32043919"
        }
      ],
      "sources": [
        {
          "url": "https://www.guatecompras.gt",
          "id": "GUATECOMPRAS",
          "name": "Sistema de Información de Contrataciones y Adquisiciones del Estado"
        }
      ],
      "dataSegmentation": {
        "criteria": [
          "añoPublicaciónConvocatoria",
          "mesPublicaciónConvocatoria"
        ],
        "id": "2022-05"
      },
      "id": "GT-NOG-17326044-T638358369500140148"
    }
  ]
}'
